class Post < ActiveRecord::Base

  attr_accessible :content, :name, :title, :date, :category_id
  belongs_to :user
  belongs_to :category
end
